﻿CREATE TABLE [dbo].[Reward]
(
	[RewardId] INT IDENTITY(1,1) NOT NULL, 
    [Amount] REAL NULL, 
    [DateRewarded] DATETIME NULL, 
    [DateClaimed] DATETIME NULL, 
    [RewardCategoryId] INT NULL,
	PRIMARY KEY CLUSTERED ([RewardId] ASC),
	CONSTRAINT [FK_dbo.Reward_dbo.RewardCategory_RewardCatId] FOREIGN KEY ([RewardCategoryId]) 
	REFERENCES [dbo].[RewardCategory]([RewardCatId]) ON DELETE CASCADE
)
