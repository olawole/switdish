﻿CREATE TABLE [dbo].[RewardCategory]
(
	[RewardCatId] INT NOT NULL, 
    [Name] NVARCHAR(50) NULL, 
    [Description] NVARCHAR(MAX) NULL, 
    [RuleId] INT NULL,
	PRIMARY KEY CLUSTERED ([RewardCatId] ASC),
    CONSTRAINT [FK_dbo.RewardCategory_dbo.AccumulationRules_RuleId] FOREIGN KEY ([RuleId]) 
	REFERENCES [dbo].[AccumulationRules]([RuleId]) ON DELETE CASCADE
)
