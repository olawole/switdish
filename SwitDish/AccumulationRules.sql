﻿CREATE TABLE [dbo].[AccumulationRules]
(
	[RuleId] INT NOT NULL, 
    [DateCreated] DATETIME NULL, 
    [Description] NVARCHAR(MAX) NULL,
	PRIMARY KEY CLUSTERED ([RuleId] ASC),
)
