﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--merge into AccumulationRules as Target
--using (values
--(1, '2017-10-01', 'First created rule'),
--(2, '2017-11-01', 'Second created rule'),
--(3, '2017-12-01', 'Third created rule')
--)
--as Source (RuleId, DateCreated, [description])
--on Target.RuleId = Source.RuleId 
--when not matched by target then 
--insert (DateCreated, [Description])
--values (DateCreated, [Description]);