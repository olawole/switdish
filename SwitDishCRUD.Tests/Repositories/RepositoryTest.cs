﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SwitDishCRUD.Repository;
using SwitDishCRUD.Models;
using System.Linq;

namespace SwitDishCRUD.Tests.Repositories
{
    /// <summary>
    /// Summary description for RepositoryTest
    /// </summary>
    [TestClass]
    public class RepositoryTest
    {
        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Insert_Reward_Must_Add_New_Reward()
        {
            using (SwitDishEntities context = new SwitDishEntities())
            {
                var unitOfWork = new UnitOfWork(context);
                int SizeBeforeInsert = unitOfWork.RewardRepository.Get().Count();
                var RewardCat = new RewardCategory
                {
                    Name = "Christmas",
                    Description = "Festivities",
                    AccumulationRule = null
                };
                var Reward = new Reward
                {
                    Amount = 25,
                    DateClaimed = new DateTime(2017, 10, 02),
                    DateRewarded = new DateTime(2017, 11, 02),
                    RewardCategory = RewardCat
                };

                unitOfWork.RewardRepository.Insert(Reward);
                unitOfWork.Save();

                int SizeAfterInsert = unitOfWork.RewardRepository.Get().Count();

                Assert.AreNotEqual(SizeBeforeInsert, SizeAfterInsert);

            }
            
        }
    }
}
