﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwitDishCRUD.Repository
{
    interface IRepository<T>
    {
        IQueryable<T> GetAll();
        Task<T> GetByID(int entityId);
        void Insert(T entity);
        void Delete(int entityId);
        void Update(T entity);
        void Save();
    }
}
