﻿using SwitDishCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SwitDishCRUD.Repository
{
    public class UnitOfWork : IDisposable
    {
        private SwitDishEntities context;
        private GenericRepository<Reward> rewardRepository;
        private GenericRepository<RewardCategory> rewardCategoryRepository;
        private GenericRepository<AccumulationRule> accumulationRuleRepository;
        private GenericRepository<RewardOrder> rewardOrderRepository;

        public UnitOfWork()
        {
            context = new SwitDishEntities();
        }

        public UnitOfWork(SwitDishEntities context)
        {
            this.context = context;
        }

        public GenericRepository<Reward> RewardRepository
        {
            get
            {
                if (this.rewardRepository == null)
                {
                    this.rewardRepository = new GenericRepository<Reward>(context);
                }
                return rewardRepository;
            }
        }

        public GenericRepository<RewardCategory> RewardCategoryRepository
        {
            get
            {
                if (this.rewardCategoryRepository == null)
                {
                    this.rewardCategoryRepository = new GenericRepository<RewardCategory>(context);
                }
                return rewardCategoryRepository;
            }
        }

        public GenericRepository<AccumulationRule> AccumulationRuleRepository
        {
            get
            {
                if (this.accumulationRuleRepository == null)
                {
                    this.accumulationRuleRepository = new GenericRepository<AccumulationRule>(context);
                }
                return accumulationRuleRepository;
            }
        }

        public GenericRepository<RewardOrder> RewardOrderRepository
        {
            get
            {
                if (this.rewardOrderRepository == null)
                {
                    this.rewardOrderRepository = new GenericRepository<RewardOrder>(context);
                }
                return rewardOrderRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    context.Dispose();
                }

                this.disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UnitOfWork() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}