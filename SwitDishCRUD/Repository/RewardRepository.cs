﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using SwitDishCRUD.Models;

namespace SwitDishCRUD.Repository
{
    public class RewardRepository : IRepository<Reward>, IDisposable
    {
        private SwitDishEntities context;

        private bool disposed = false;

        public RewardRepository(SwitDishEntities context)
        {
            this.context = context;
        }
        public async void Delete(int entityId)
        {
            Reward reward = await context.Rewards.FindAsync(entityId);
            context.Rewards.Remove(reward);
        }

        public IQueryable<Reward> GetAll()
        {
            return context.Rewards;
        }

        public async Task<Reward> GetByID(int entityId)
        {
            return await context.Rewards.FindAsync(entityId);
        }

        public void Insert(Reward reward)
        {
            context.Rewards.Add(reward);
        }

        public async void Save()
        {
            await context.SaveChangesAsync();
        }

        public void Update(Reward reward)
        {
            context.Entry(reward).State = System.Data.Entity.EntityState.Modified;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}