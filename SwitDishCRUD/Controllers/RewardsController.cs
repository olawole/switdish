﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SwitDishCRUD.Models;
using SwitDishCRUD.Repository;

namespace SwitDishCRUD.Controllers
{
    public class RewardsController : ApiController
    {
        private UnitOfWork unitOfWork;

        public RewardsController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        // GET: api/Rewards
        public IEnumerable<Reward> GetRewards()
        {
            return unitOfWork.RewardRepository.Get();
        }

        // GET: api/Rewards/5
        [ResponseType(typeof(Reward))]
        public async Task<IHttpActionResult> GetReward(int id)
        {
            Reward reward = await unitOfWork.RewardRepository.GetByID(id);
            if (reward == null)
            {
                return NotFound();
            }

            return Ok(reward);
        }

        // PUT: api/Rewards/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutReward(int id, Reward reward)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != reward.RewardId)
            {
                return BadRequest();
            }

            try
            {
                unitOfWork.RewardRepository.Update(reward);
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RewardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Rewards
        [ResponseType(typeof(Reward))]
        public IHttpActionResult PostReward(Reward reward)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            unitOfWork.RewardRepository.Insert(reward);
            unitOfWork.Save();

            return CreatedAtRoute("DefaultApi", new { id = reward.RewardId }, reward);
        }

        // DELETE: api/Rewards/5
        [ResponseType(typeof(Reward))]
        public async Task<IHttpActionResult> DeleteReward(int id)
        {
            Reward reward = await unitOfWork.RewardRepository.GetByID(id);
            if (reward == null)
            {
                return NotFound();
            }

            unitOfWork.RewardRepository.Delete(id);
            unitOfWork.Save();

            return Ok(reward);
        }

        private bool RewardExists(int id)
        {
            return unitOfWork.RewardRepository.Get().Count(e => e.RewardId == id) > 0;
        }
    }
}