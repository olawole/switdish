﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SwitDishCRUD.Models;
using SwitDishCRUD.Repository;

namespace SwitDishCRUD.Controllers
{
    public class RewardOrdersController : ApiController
    {
        private UnitOfWork unitOfWork;

        public RewardOrdersController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        // GET: api/RewardOrders
        public IEnumerable<RewardOrder> GetRewardOrders()
        {
            return unitOfWork.RewardOrderRepository.Get();
        }

        // GET: api/RewardOrders/5
        [ResponseType(typeof(RewardOrder))]
        public async Task<IHttpActionResult> GetRewardOrder(int id)
        {
            RewardOrder rewardOrder = await unitOfWork.RewardOrderRepository.GetByID(id);
            if (rewardOrder == null)
            {
                return NotFound();
            }

            return Ok(rewardOrder);
        }

        // PUT: api/RewardOrders/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRewardOrder(int id, RewardOrder rewardOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rewardOrder.Id)
            {
                return BadRequest();
            }
            try
            {
                unitOfWork.RewardOrderRepository.Update(rewardOrder);
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RewardOrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RewardOrders
        [ResponseType(typeof(RewardOrder))]
        public IHttpActionResult PostRewardOrder(RewardOrder rewardOrder)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                unitOfWork.RewardOrderRepository.Insert(rewardOrder);
                unitOfWork.Save();
            }
            catch (DbUpdateException)
            {
                if (RewardOrderExists(rewardOrder.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = rewardOrder.Id }, rewardOrder);
        }

        // DELETE: api/RewardOrders/5
        [ResponseType(typeof(RewardOrder))]
        public async Task<IHttpActionResult> DeleteRewardOrder(int id)
        {
            RewardOrder rewardOrder = await unitOfWork.RewardOrderRepository.GetByID(id);
            if (rewardOrder == null)
            {
                return NotFound();
            }

            unitOfWork.RewardOrderRepository.Delete(id);
            unitOfWork.Save();

            return Ok(rewardOrder);
        }

        private bool RewardOrderExists(int id)
        {
            return unitOfWork.RewardOrderRepository.Get().Count(e => e.Id == id) > 0;
        }
    }
}