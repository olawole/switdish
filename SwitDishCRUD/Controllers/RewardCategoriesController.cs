﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SwitDishCRUD.Models;
using SwitDishCRUD.Repository;

namespace SwitDishCRUD.Controllers
{
    public class RewardCategoriesController : ApiController
    {
        private UnitOfWork unitOfWork;

        public RewardCategoriesController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        // GET: api/RewardCategories
        public IEnumerable<RewardCategory> GetRewardCategories()
        {
            return unitOfWork.RewardCategoryRepository.Get();
        }

        // GET: api/RewardCategories/5
        [ResponseType(typeof(RewardCategory))]
        public async Task<IHttpActionResult> GetRewardCategory(int id)
        {
            RewardCategory rewardCategory = await unitOfWork.RewardCategoryRepository.GetByID(id);
            if (rewardCategory == null)
            {
                return NotFound();
            }

            return Ok(rewardCategory);
        }

        // PUT: api/RewardCategories/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutRewardCategory(int id, RewardCategory rewardCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != rewardCategory.RewardCatId)
            {
                return BadRequest();
            }

            try
            {
                unitOfWork.RewardCategoryRepository.Update(rewardCategory);
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RewardCategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/RewardCategories
        [ResponseType(typeof(RewardCategory))]
        public IHttpActionResult PostRewardCategory(RewardCategory rewardCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                unitOfWork.RewardCategoryRepository.Insert(rewardCategory);
                unitOfWork.Save();
            }
            catch (DbUpdateException)
            {
                if (RewardCategoryExists(rewardCategory.RewardCatId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = rewardCategory.RewardCatId }, rewardCategory);
        }

        // DELETE: api/RewardCategories/5
        [ResponseType(typeof(RewardCategory))]
        public async Task<IHttpActionResult> DeleteRewardCategory(int id)
        {
            RewardCategory rewardCategory = await unitOfWork.RewardCategoryRepository.GetByID(id);
            if (rewardCategory == null)
            {
                return NotFound();
            }

            unitOfWork.RewardCategoryRepository.Delete(rewardCategory);
            unitOfWork.Save();

            return Ok(rewardCategory);
        }

        private bool RewardCategoryExists(int id)
        {
            return unitOfWork.RewardCategoryRepository.Get().Count(e => e.RewardCatId == id) > 0;
        }
    }
}