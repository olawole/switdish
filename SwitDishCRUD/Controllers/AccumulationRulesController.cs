﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using SwitDishCRUD.Models;
using SwitDishCRUD.Repository;

namespace SwitDishCRUD.Controllers
{
    public class AccumulationRulesController : ApiController
    {
        private UnitOfWork unitOfWork;

        public AccumulationRulesController()
        {
            this.unitOfWork = new UnitOfWork();
        }

        // GET: api/AccumulationRules
        public IEnumerable<AccumulationRule> GetAccumulationRules()
        {
            return unitOfWork.AccumulationRuleRepository.Get();
        }

        // GET: api/AccumulationRules/5
        [ResponseType(typeof(AccumulationRule))]
        public async Task<IHttpActionResult> GetAccumulationRule(int id)
        {
            AccumulationRule accumulationRule = await unitOfWork.AccumulationRuleRepository.GetByID(id);
            if (accumulationRule == null)
            {
                return NotFound();
            }

            return Ok(accumulationRule);
        }

        // PUT: api/AccumulationRules/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutAccumulationRule(int id, AccumulationRule accumulationRule)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != accumulationRule.RuleId)
            {
                return BadRequest();
            }

            try
            {
                unitOfWork.AccumulationRuleRepository.Update(accumulationRule);
                unitOfWork.Save();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AccumulationRuleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/AccumulationRules
        [ResponseType(typeof(AccumulationRule))]
        public IHttpActionResult PostAccumulationRule(AccumulationRule accumulationRule)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                unitOfWork.AccumulationRuleRepository.Insert(accumulationRule);
                unitOfWork.Save();
            }
            catch (DbUpdateException)
            {
                if (AccumulationRuleExists(accumulationRule.RuleId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = accumulationRule.RuleId }, accumulationRule);
        }

        // DELETE: api/AccumulationRules/5
        [ResponseType(typeof(AccumulationRule))]
        public async Task<IHttpActionResult> DeleteAccumulationRule(int id)
        {
            AccumulationRule accumulationRule = await unitOfWork.AccumulationRuleRepository.GetByID(id);
            if (accumulationRule == null)
            {
                return NotFound();
            }

            unitOfWork.AccumulationRuleRepository.Delete(accumulationRule);
            unitOfWork.Save();

            return Ok(accumulationRule);
        }

        private bool AccumulationRuleExists(int id)
        {
            return unitOfWork.AccumulationRuleRepository.Get().Count(e => e.RuleId == id) > 0;
        }

    }
}